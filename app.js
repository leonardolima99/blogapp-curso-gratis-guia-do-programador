// Carregando módulos
const express = require('express')
const handlebars = require('express-handlebars')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const admin = require('./routes/admin')
const usuario = require('./routes/usuario')
const path = require('path')
const session = require('express-session')
const flash = require('connect-flash')
require('./models/Postagem')
const Postagem = mongoose.model('postagens')
require('./models/Categoria')
const Categoria = mongoose.model('categorias')
const passport = require('passport')
require('./config/auth')(passport)
const db = require('./config/db')

const app = express()

// Configurações
// Sessão
app.use(session({
  secret: 'cursodenode',
  resave: true,
  saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

// Middleware
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg')
  res.locals.error_msg = req.flash('error_msg')
  res.locals.error = req.flash('error')
  res.locals.user = req.user || null
  next()
})

// Body Parser
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

// Handlebars
const hbs = handlebars.create({
  defaultLayout: 'main',

  // Custom helpers
  helpers: {
    is: function(id, catId, options) {
      if (catId || catId == id) return options.fn(this)
      return options.inverse(this)
    },
    isNot: function(v1, v2, options) {
      if (v1 != v2) return options.fn(this)
      return options.inverse(this)
    }
  }
})
app.engine('handlebars', hbs.engine)
app.set('view engine', 'handlebars')
// Helper igualdade
// handlebars.registerHelper('is', function(v1, v2, options) {
//   if (v1 === v2) return options.fn(this)
//   return options.inverse(this)
// })
// handlebars.registerHelper('isNot', function(v1, v2, options) {
//   if (v1 !== v2) return options.fn(this)
//   return options.inverse(this)
// })

// Mongoose
mongoose.Promise = global.Promise
// mongoose.connect('mongodb://30b44ce625ce141c66846a69a07911fd:123456@10a.mongo.evennode.com:27017,10b.mongo.evennode.com:27017/30b44ce625ce141c66846a69a07911fd?replicaSet=us-10', {
// mongoose.connect('mongodb+srv://dbRyszol:05091993a@cluster0-yeubg.mongodb.net/blogapp', {
mongoose.connect(db.mongoURI, {
  useNewUrlParser: true
}).then(() => {
  console.log('Conectado ao mongo');
}).catch(err => {
  console.log('Erro ao se conectar: ' + err);
})

// Public
app.use(express.static(path.join(__dirname, 'public')))


// Rotas
app.get('/', (req, res) => {
  Postagem.find().populate('categoria').sort({data: -1}).then(postagens => {
    res.render('index', {postagens})
  }).catch(err => {
    req.flash('error_msg', 'Houve um erro interno!')
    res.redirect('/404')
  })
})
app.get('/post/:slug', (req, res) => {
  Postagem.findOne({'slug': req.params.slug}).populate('categoria')
    .then(post => {
      if (post) {
        res.render('postagens/index', {post})
      } else {
        req.flash('error_msg', 'Esta postagem não existe!')
        res.redirect('/')
      }
    })
    .catch(err => {
      req.flash('error_msg', 'Houve um erro interno.')
      res.redirect('/')
    })
})
app.get('/categorias', (req, res) => {
  Categoria.find().then(categorias => {
    res.render('categorias/index', {categorias})
  }).catch(err => {
    res.redirect('/')
    req.flash('error_msg', 'Houve um erro interno ao listar as categorias!')
  })
})
app.get('/categoria/:slug', (req, res) => {
  Categoria.findOne({slug: req.params.slug}).then(categoria => {
    if (categoria) {
      Postagem.find({'categoria': categoria.id}).then(posts => {
        console.log(posts);
        res.render('postagens/filtradas', {posts,categoria})
      }).catch(err => {
        req.flash('error_msg', 'Houve um erro ao listar as postagens.')
        res.redirect('/')
      })
    } else {
      req.flash('error_msg', 'Esta categoria não existe.')
      res.redirect('/')
    }
  }).catch(err => {
    req.flash('error_msg', `Houve um erro ao carregar a página. | ${err}`)
    res.redirect('/')
  })
})
app.get('/404', (req, res) => {
  res.send('Erro 404!')
})
app.get('/posts', (req, res) => {
  res.send('Lista de posts')
})
app.use('/admin', admin)
app.use('/usuario', usuario)


// Outros
const PORT = process.env.PORT || 3000
const host = 'http://localhost'
  app.listen(PORT, () => {
  console.log('Servidor rodando em ' + host + ':' + PORT + '/admin/postagens')
})
