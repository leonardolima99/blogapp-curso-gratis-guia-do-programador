const express = require('express');
const router = express.Router()
const mongoose = require('mongoose')
require('../models/Categoria')
require('../models/Postagem')
const Categoria = mongoose.model('categorias')
const Postagem = mongoose.model('postagens')
const { isAdmin } = require('../helpers/isAdmin')
 
const order = ['data', -1]

const model_select = {
  nome: false,
  slug: false,
  data: 'selected'
}

const model_check = {
  asc: false,
  desc: 'checked'
}

function reativeSort() {
  switch(order[0]) {
    case 'data':
      model_select.nome = false
      model_select.slug = false
      model_select.data = 'selected'
      break;

    case 'nome':
      model_select.nome = 'selected'
      model_select.slug = false
      model_select.data = false
      break;

    case 'slug':
      model_select.nome = false
      model_select.slug = 'selected'
      model_select.data = false
      break;

    default:
      console.log('Select não definido!')
  }
  switch (order[1]) {
    case -1:
      model_check.asc = false
      model_check.desc = 'checked'
      break;

    case 1:
      model_check.asc = 'checked'
      model_check.desc = false
      break;

    default:
      console.log('Radio não definido!');
  }
}

function validate(value, type) {
  var erros = []

  if (type == 'categoria') { // Validação da categoria
    if (!value.nome || typeof value.nome == undefined || value.nome == null) {
      erros.push({texto: "Nome inválido."})
    } else if (value.nome.length < 2) {
      erros.push({texto: "Nome muito pequeno."})
    }
    if (!value.slug || typeof value.slug == undefined || value.slug == null) {
      erros.push({texto: "Slug inválido."})
    }
  } else if (type == 'postagem') { // Validação da postagem
    if (!value.titulo || typeof value.titulo == undefined || value.titulo == null) {
      erros.push({texto: "Título inválido."})
    } else if (value.titulo.length < 5) {
      erros.push({texto: "Título muito pequeno."})
    }
    if (!value.slug || typeof value.slug == undefined || value.slug == null) {
      erros.push({texto: "Slug inválido."})
    }
    if (value.categoria == 0) {
      erros.push({texto: "Registre uma categoria."})
    }
    if (!value.descricao || typeof value.descricao == undefined || value.descricao == null) {
      erros.push({texto: "Descrição inválida."})
    } else if (value.descricao.length < 5) {
      erros.push({texto: "Descrição muito pequena."})
    }
    if (!value.conteudo || typeof value.conteudo == undefined || value.descricao == null) {
      erros.push({texto: "Conteúdo inválido."})
    } else if (value.conteudo.length < 5) {
      erros.push({texto: "Conteúdo muito pequeno."})
    }
  } else {
    console.log('Tipo não existe!');
  }

  return erros.reverse()
}

function resetOrder(value) {
  order.pop()
  order.pop()
  order.push(value.select_order)
  order.push(parseInt(value.radio_order))
}

router.get('/', isAdmin, (req, res) => {
  res.render('admin/index')
})

router.get('/posts', isAdmin, (req, res) => {
  res.send('Pagina de posts')
})

router.post('/categorias', isAdmin, (req, res) => {
  resetOrder(req.body)
  console.log(order);
  res.redirect('/admin/categorias')
})

router.get('/categorias', isAdmin, (req, res) => {
  Categoria.find().sort([order]).then(categorias => {
    reativeSort()
    res.render('admin/categorias', {
      categorias,
      select: model_select,
      check: model_check
    })
  }).catch(err => {
    req.flash('error_msg', 'Houve um erro ao listar as categorias!')
  })
})

router.get('/categorias/add', isAdmin, (req, res) => {
  res.render('admin/addcategoria')
})

router.post('/categorias/nova', isAdmin, (req, res) => {

  var erros = validate(req.body, 'categoria')

  if (erros.length > 0) {
    res.render('admin/addcategoria', {erros})
  } else {
    const novaCategoria = {
      nome: req.body.nome,
      slug: req.body.slug,
      data: new Date()
    }

    new Categoria(novaCategoria).save().then(() => {
      console.log('Categoria salva com sucesso')
      req.flash('success_msg', 'Categoria salva com sucesso!')
      res.redirect('/admin/categorias')
    }).catch(err => {
      console.log('Erro ao salvar categoria! ' + err)
      req.flash('error_msg', 'Erro ao salvar a categoria: ' + err)
    })
  }

})

var placeholder_edit = []

// function editarCategoria(value) {
//   let id = value.id
//   let nome = value.nome
//   let slug = value.slug
//   placeholder_edit.push(nome)
//   placeholder_edit.push(slug)
//   return {
//     id: id,
//     nome: nome,
//     slug: slug
//   }
// }
function editarCategoria(value) {
  let id = value.id
  let nome = value.nome
  let slug = value.slug
  placeholder_edit.push(nome)
  placeholder_edit.push(slug)
  console.log(placeholder_edit)
  return [
    id,
    placeholder_edit[0],
    placeholder_edit[1]
  ]
}
function editarPostagem(value) {
  let id = value.id
}
router.get('/categorias/edit/:id', isAdmin, (req, res) => {
  Categoria.findOne({"_id": req.params.id}).then((categoria) => {
    res.render('admin/editcategoria', categoria)
  }).catch(err => {
    req.flash('error_msg', 'Esta categoria não existe!')
    // console.log('Houve um erro: ' + err)
    res.redirect('/admin/categorias')
  })
})

router.post('/categorias/edit', isAdmin, (req, res) => {
  var erros = validate(req.body, 'categoria')
  const editarCategoria_dados = {
    id: req.body.id,
    nome: req.body.nome,
    slug: req.body.slug
  }
  console.log('Dados: ' + editarCategoria_dados);
  console.log('Erros: ' + erros);
  var valores = editarCategoria(req.body)
  valores.push(erros)
  console.log('Valores: ' + valores);
  if (erros.length > 0) {
    res.render('admin/editcategoria', {
      id: valores[0],
      nome: valores[1],
      slug: valores[2],
      erros: valores[3]
    })
  } else {
    Categoria.updateOne(
      {
        "_id": req.body.id
      },
      {
        $set: {
          nome: req.body.nome,
          slug: req.body.slug,
          data: new Date()
        }
      }
    ).then(() => {
      console.log("Categoria editada com sucesso!")
      req.flash('success_msg', 'Categoria editada com sucesso!')
      res.redirect('/admin/categorias')
    }).catch(err => {
      console.log("Houve um erro: " + err)
      req.flash('error_msg', 'Erro ao editar categoria: ' + err)
    })
  }
})

router.post('/categorias/delete', isAdmin, (req, res) => {
  Categoria.deleteOne({"_id": req.body.id}).then(() => {
    req.flash('success_msg', 'Categoria deletada com sucesso!')
    // req.toastr.success('Categoria deletada com sucesso!')
    res.redirect('/admin/categorias')
  }).catch(err => {
    req.flash('error_msg', 'Esta categoria não existe!')
    res.redirect('/admin/categorias')
  })
})

// Rotas Postagens
router.post('/postagens', isAdmin, (req, res) => {
  resetOrder(req.body)
  console.log(order)
  res.redirect('/admin/postagens')
})

router.get('/postagens', isAdmin, (req, res) => {
  Postagem.find().sort([order]).then(postagens => {
    reativeSort()
    res.render('admin/postagens', {
      postagens,
      select: model_select,
      check: model_check
    })
  }).catch(err => {
    req.flash('error_msg', 'Houve um erro ao listar as postagens!')
  })
})

router.get('/postagens/add', isAdmin, (req, res) => {
  Categoria.find().then(categorias => {
    res.render('admin/addpostagem', {categorias})
  }).catch(err => {
    req.flash('error_msg', 'Houve um erro ao carregar o formulário! ' + err)
  })
})

router.post('/postagens/nova', isAdmin, (req, res) => {
  var erros = validate(req.body, 'postagem')
  console.log(erros);
  if (erros.length > 0) {
    res.render('admin/addpostagem', {erros})
  } else {
    const novaPostagem = {
      titulo: req.body.titulo,
      slug: req.body.slug,
      categoria: req.body.categoria,
      descricao: req.body.descricao,
      conteudo: req.body.conteudo,
      data: new Date()
    }

    new Postagem(novaPostagem).save().then(() => {
      console.log('Postagem salva com sucesso')
      req.flash('success_msg', 'Postagem salva com sucesso!')
      res.redirect('/admin/postagens')
    }).catch(err => {
      console.log('Erro ao salvar postagem! ' + err)
      req.flash('error_msg', 'Erro ao salvar a postagem: ' + err)
    })
    // res.redirect('/admin/postagens')
  }
})

var placeholder_edit_post = []

function editarPostagem1(value, value2) {
  let id = value.id
  let titulo = value.titulo
  let slug = value.slug
  let categoria = value.categoria
  let descricao = value.descricao
  let conteudo = value.conteudo
  placeholder_edit_post.push(titulo)
  placeholder_edit_post.push(slug)
  placeholder_edit_post.push(categoria)
  placeholder_edit_post.push(descricao)
  placeholder_edit_post.push(conteudo)
  return {
    id,
    titulo,
    slug,
    categoria,
    value2,
    descricao,
    conteudo
  }
}

router.get('/postagens/edit/:id', isAdmin, (req, res) => {
  Postagem.findOne({"_id": req.params.id}).then(postagem => {
    Categoria.find().then(categorias => {
      Categoria.findOne({"_id": postagem.categoria})
        .then(categoriaEscolhida => {
          console.log(categoriaEscolhida.id, typeof categoriaEscolhida.id)
          let select_filter = []
          categorias.forEach(cat => {
            if (cat.id !== categoriaEscolhida.id) {
              select_filter.push({
                id: cat.id,
                nome: cat.nome,
                selected: false
              })
            } else {
              select_filter.push({
                id: cat.id,
                nome: cat.nome,
                selected: true
              })
            }
          })

          console.log(select_filter)

          res.render('admin/editpostagem', {
            id: postagem.id,
            titulo: postagem.titulo,
            slug: postagem.slug,
            categoriaId: categoriaEscolhida.id,
            categoriaNome: categoriaEscolhida.nome,
            select_filter,
            descricao: postagem.descricao,
            conteudo: postagem.conteudo
          })
        }).catch(err => {
          req.flash('error_msg', 'A categoria da postagem não existe mais, ' +
                                  'então você não pode editar a postagem!')
          res.redirect('/admin/postagens');
          console.log('Não pode encontrar a categoria do post!')
        })
    }).catch(err => {
      req.flash('error_msg', 'Houve um erro ao buscar as categorias!')
      res.redirect('/admin/postagens');
      console.log('Erro ao buscar as categorias!' + err)
    })
  }).catch(err => {
    req.flash('error_msg', 'Esta postagem não existe!')
    res.redirect('/admin/postagens')
  })
})

router.post('/postagens/edit', isAdmin, (req, res) => {
  var erros = validate(req.body, 'postagem')
  if (erros.length > 0) {
    console.log('Deu ruim ai!' + erros);
  } else {
    Postagem.updateOne(
      {
        "_id": req.body.id
      },
      {
        $set: {
          titulo: req.body.titulo,
          slug: req.body.slug,
          categoria: req.body.categoria,
          descricao: req.body.descricao,
          conteudo: req.body.conteudo,
          data: new Date()
        }
      }
    ).then(postagem => {
      console.log('Postagem editada com sucesso!')
      req.flash('success_msg', 'Postagem editada com sucesso!')
      res.redirect('/admin/postagens');
    }).catch(err => {
      req.flash('error_msg', 'Houve um erro ao salvar a edição!')
      res.redirect('/admin/postagens');
    })
  }
})

router.post('/postagens/delete', isAdmin, (req, res) => {
  Postagem.deleteOne({"_id": req.body.id}).then(() => {
    req.flash('success_msg', 'Postagem deletada com sucesso!')
    res.redirect('/admin/postagens')
  }).catch(err => {
    req.flash('error_msg', 'Esta postagem não existe!')
    res.redirect('/admin/postagens');
  })
})

module.exports = router;
