const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
require('../models/Usuario')
const Usuario = mongoose.model('usuarios')
const bcrypt = require('bcryptjs')
const passport = require('passport')
const { isLogged } = require('../helpers/isLogged')
const { isNotLogged } = require('../helpers/isNotLogged')

router.get('/registro', isLogged, (req, res) => {
  res.render('usuarios/registro')
})

router.post('/registro', isLogged, (req, res) => {
  var erros = []

  if (!req.body.nome || req.body.nome == undefined || req.body.nome == null) {
    erros.push({texto: "Nome inválido."})
  }
  
  if (!req.body.email || req.body.email == undefined || req.body.email == null) {
    erros.push({texto: "Email inválido."})
  }
  
  if (!req.body.senha || req.body.senha == undefined || req.body.senha == null) {
    erros.push({texto: "Senha inválida."})
  }

  if (req.body.senha.length < 4) {
    erros.push({texto: "Senha muito curta."})
  }

  if (req.body.senha != req.body.senha2) {
    erros.push({texto: "As senhas são diferentes."})
  }

  if (erros.length > 0) {

    res.render('usuarios/registro', {erros})

  } else {

    Usuario.findOne({email: req.body.email}).then(usuario => {
      if (usuario) {
        req.flash('error_msg', 'Já existe uma conta com esse email.')
        res.redirect('/usuario/registro')
      } else {
        
        const { nome, email, senha } = req.body
        var isAdmin = req.body.isAdmin
        if (isAdmin == 'on') {
          isAdmin = true
        } else {
          isAdmin = false
        }
        const novoUsuario = new Usuario({nome, email, senha, isAdmin})

        bcrypt.genSalt(10, (erro, salt) => {
          bcrypt.hash(novoUsuario.senha, salt, (erro, hash) => {
            if (erro) {
              req.flash('error_msg', 'Houve um erro durante o salvamento d usuário')
              res.redirect('/')
            }

            novoUsuario.senha = hash

            novoUsuario.save().then(() => {
              req.flash('success_msg', 'Usuário criado com sucesso!')
              res.redirect('/')
            }).catch(error => {
              req.flash('error_msg', 'Houve um erro ao criar o usuario. ' + error)
              res.redirect('/')
            })

          })
        })

      }
    }).catch(error => {
      req.flash('error_msg', 'Houve um erro interno.')
      res.redirect('/')
    })

  }
  
})

router.get('/login', isLogged, (req, res) => {
  res.render('usuarios/login')
})

router.post('/login', isLogged, (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/usuario/login',
    failureFlash: true
  })(req, res, next)
})

router.get('/logout', isNotLogged, (req, res) => {
  req.logout()
  req.flash('success_msg', 'Deslogado.')
  res.redirect('/')
})

module.exports = router