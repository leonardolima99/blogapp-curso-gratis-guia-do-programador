if (process.env.NODE_ENV == 'production') {
  module.exports = {
    mongoURI: 'mongodb://30b44ce625ce141c66846a69a07911fd:123456@10a.mongo.evennode.com:27017,10b.mongo.evennode.com:27017/30b44ce625ce141c66846a69a07911fd?replicaSet=us-10'
  } 
} else {
  module.exports = {
    mongoURI: 'mongodb://localhost:27017/blogapp'
  }
}