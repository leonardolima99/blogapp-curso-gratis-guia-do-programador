module.exports = {
  isLogged: function(req, res, next) {
    if (!req.isAuthenticated()) {
      return next()
    }

    req.flash('error_msg', 'Você já está logado!')
    res.redirect('/')
  }
}
